package com.tsc.jarinchekhina.tm.dto;

import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "tm_user")
@NoArgsConstructor
public class UserDTO extends AbstractEntityDTO {

    public UserDTO(@Nullable final User user) {
        this.setId(user.getId());
        this.login = user.getLogin();
        this.passwordHash = user.getPasswordHash();
        this.email = user.getEmail();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.middleName = user.getMiddleName();
        this.role = user.getRole();
        this.locked = user.isLocked();
    }

    @Column
    @NotNull
    private String login;

    @Column
    @NotNull
    private String passwordHash;

    @Column
    @Nullable
    private String email;

    @Column
    @Nullable
    private String firstName;

    @Column
    @Nullable
    private String lastName;

    @Column
    @Nullable
    private String middleName;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

    private boolean locked = false;

}
