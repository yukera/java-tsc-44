package com.tsc.jarinchekhina.tm.model;

import com.tsc.jarinchekhina.tm.dto.ProjectDTO;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "tm_project")
public class Project extends AbstractBusinessEntity {

    @Column
    @NotNull
    private String name;

    @Column
    @Nullable
    private String description;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Column
    @Nullable
    private Date dateStart;

    @Column
    @Nullable
    private Date dateFinish;

    @Column
    @Nullable
    private Date created = new Date();

    @Nullable
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    @Nullable
    public static ProjectDTO toDTO(@Nullable final Project project) {
        if (project == null) return null;
        return new ProjectDTO(project);
    }

    @NotNull
    public static List<ProjectDTO> toDTO(@Nullable final List<Project> projectList) {
        if (DataUtil.isEmpty(projectList)) return Collections.emptyList();
        @NotNull final List<ProjectDTO> result = new ArrayList<>();
        for (@Nullable final Project project : projectList) {
            if (project == null) continue;
            result.add(Project.toDTO(project));
        }
        return result;
    }

}
