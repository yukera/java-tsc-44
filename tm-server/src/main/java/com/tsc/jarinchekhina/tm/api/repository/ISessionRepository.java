package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.model.Session;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    void clear();

    void removeById(@NotNull String id);

    @NotNull
    List<Session> findAll();

    @NotNull
    List<Session> findByUserId(@NotNull String userId);

    @NotNull
    Session findById(@NotNull String id);

}
