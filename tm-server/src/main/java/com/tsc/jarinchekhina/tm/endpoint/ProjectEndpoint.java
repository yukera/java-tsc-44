package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.api.endpoint.IProjectEndpoint;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.dto.ProjectDTO;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.model.Project;
import com.tsc.jarinchekhina.tm.model.Session;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void changeProjectStatusById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getProjectService().changeProjectStatusById(session.getUser().getId(), id, status);
    }

    @Override
    @WebMethod
    public void changeProjectStatusByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getProjectService().changeProjectStatusByIndex(session.getUser().getId(), index, status);
    }

    @Override
    @WebMethod
    public void changeProjectStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getProjectService().changeProjectStatusByName(session.getUser().getId(), name, status);
    }

    @Override
    @WebMethod
    public void clearProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getProjectService().clear(session.getUser().getId());
    }

    @Override
    @WebMethod
    public void createProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getProjectService().create(session.getUser().getId(), name);
    }

    @Override
    @WebMethod
    public void createProjectWithDescription(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getProjectService().create(session.getUser().getId(), name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDTO> findAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        return Project.toDTO(serviceLocator.getProjectService().findAll(session.getUser().getId()));
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO findProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        return Project.toDTO(serviceLocator.getProjectService().findById(session.getUser().getId(), id));
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO findProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        return Project.toDTO(serviceLocator.getProjectService().findByIndex(session.getUser().getId(), index));
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO findProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        return Project.toDTO(serviceLocator.getProjectService().findByName(session.getUser().getId(), name));
    }

    @Override
    @WebMethod
    public void finishProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getProjectService().finishProjectById(session.getUser().getId(), id);
    }

    @Override
    @WebMethod
    public void finishProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getProjectService().finishProjectByIndex(session.getUser().getId(), index);
    }

    @Override
    @WebMethod
    public void finishProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getProjectService().finishProjectByName(session.getUser().getId(), name);
    }

    @Override
    @WebMethod
    public void removeProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getProjectTaskService().removeProjectById(session.getUser().getId(), id);
    }

    @Override
    @WebMethod
    public void removeProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        @NotNull Project project = serviceLocator.getProjectService().findByIndex(session.getUser().getId(), index);
        serviceLocator.getProjectTaskService().removeProjectById(session.getUser().getId(), project.getId());
    }

    @Override
    @WebMethod
    public void removeProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        @NotNull Project project = serviceLocator.getProjectService().findByName(session.getUser().getId(), name);
        serviceLocator.getProjectTaskService().removeProjectById(session.getUser().getId(), project.getId());
    }

    @Override
    @WebMethod
    public void startProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getProjectService().startProjectById(session.getUser().getId(), id);
    }

    @Override
    @WebMethod
    public void startProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getProjectService().startProjectByIndex(session.getUser().getId(), index);
    }

    @Override
    @WebMethod
    public void startProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getProjectService().startProjectByName(session.getUser().getId(), name);
    }

    @Override
    @WebMethod
    public void updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getProjectService().updateProjectById(session.getUser().getId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getProjectService().updateProjectByIndex(session.getUser().getId(), index, name, description);
    }

}
