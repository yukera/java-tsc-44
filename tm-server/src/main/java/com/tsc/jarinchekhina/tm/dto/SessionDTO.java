package com.tsc.jarinchekhina.tm.dto;

import com.tsc.jarinchekhina.tm.model.Session;
import com.tsc.jarinchekhina.tm.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "tm_session")
public class SessionDTO extends AbstractEntityDTO implements Cloneable {

    public SessionDTO(@Nullable final Session session) {
        this.setId(session.getId());
        @Nullable final User user = session.getUser();
        if (user != null) this.userId = user.getId();
        this.timestamp = session.getTimestamp();
        this.signature = session.getSignature();
    }

    @Column
    @Nullable
    private Long timestamp;

    @Column
    @Nullable
    private String userId;

    @Column
    @Nullable
    private String signature;

    @Nullable
    @Override
    public SessionDTO clone() {
        try {
            return (SessionDTO) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

}
