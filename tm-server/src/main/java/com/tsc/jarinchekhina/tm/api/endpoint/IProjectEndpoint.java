package com.tsc.jarinchekhina.tm.api.endpoint;

import com.tsc.jarinchekhina.tm.api.IEndpoint;
import com.tsc.jarinchekhina.tm.dto.ProjectDTO;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.model.Project;
import com.tsc.jarinchekhina.tm.model.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectEndpoint extends IEndpoint<Project> {

    void changeProjectStatusById(@Nullable Session session, @Nullable String id, @Nullable Status status);

    void changeProjectStatusByIndex(@Nullable Session session, @Nullable Integer index, @Nullable Status status);

    void changeProjectStatusByName(@Nullable Session session, @Nullable String name, @Nullable Status status);

    void clearProjects(@Nullable Session session);

    void createProject(@Nullable Session session, @Nullable String name);

    void createProjectWithDescription(
            @Nullable Session session,
            @Nullable String name,
            @Nullable String description);

    @NotNull
    List<ProjectDTO> findAllProjects(@Nullable Session session);

    @NotNull
    ProjectDTO findProjectById(@Nullable Session session, @Nullable String id);

    @NotNull
    ProjectDTO findProjectByIndex(@Nullable Session session, @Nullable Integer index);

    @NotNull
    ProjectDTO findProjectByName(@Nullable Session session, @Nullable String name);

    void finishProjectById(@Nullable Session session, @Nullable String id);

    void finishProjectByIndex(@Nullable Session session, @Nullable Integer index);

    void finishProjectByName(@Nullable Session session, @Nullable String name);

    void removeProjectById(@Nullable Session session, @Nullable String id);

    void removeProjectByIndex(@Nullable Session session, @Nullable Integer index);

    void removeProjectByName(@Nullable Session session, @Nullable String name);

    void startProjectById(@Nullable Session session, @Nullable String id);

    void startProjectByIndex(@Nullable Session session, @Nullable Integer index);

    void startProjectByName(@Nullable Session session, @Nullable String name);

    void updateProjectById(
            @Nullable Session session,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    void updateProjectByIndex(
            @Nullable Session session,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

}
